# Wiki-Game-Tp

## Installation
__*this project is running on* [python@3.11](https://peps.python.org/pep-0664/)__

__Homebrew (MacOs):__

`brew install python@3.11`

__Manually :__

[version 3.11 Windaube](https://www.python.org/downloads/windows/)

[version 3.11 Linux](https://www.python.org/downloads/source/)

__Packages :__ 
 > pip install -r requirements.txt

## Tips
> Launch the script from Game.py

