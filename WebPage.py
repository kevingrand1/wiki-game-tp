from urllib.request import Request, urlopen
from urllib.parse import unquote
from bs4 import BeautifulSoup
import certifi

black_list = {
    'None',
    'Modifier la section',
    'Aide:',
    'Catégorie:',
    'Portail:',
    'Documentation du modèle',
    'Voir et modifier',
    'Si ce bandeau n\'est plus pertinent',
    'Consultez la documentation du modèle',
    'Discussion:',
    'Wikipédia:',
    'Projet:',
    'Modèle:',
    'Spécial:',
    'Cette page est en semi-protection longue',
    'Le titre de cette page ne peut être modifié',
    'Fichier:'
}


class WebPage:
    def __init__(self):
        self._soup = None
        self._id_link = None
        self._title = None
        self._url = None

    def get_page(self, target=None):
        if not target:
            target = "https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Page_au_hasard"
        else:
            target = target
        req = Request(
            url=target,
            headers={'User-Agent': 'Mozilla/5.0'}
        )
        webpage = urlopen(req, cafile=certifi.where()).read()
        self._soup = BeautifulSoup(webpage, 'html.parser')
        raw_result = str(self._soup.find('link', {"rel": "canonical"}))
        self._url = raw_result[12:-19]
        self._title = unquote(raw_result[42:-19].replace('_', ' '))

        return {
            "url": self._url,
            "title": self._title
        }

    def get_all_links(self):
        links = []
        id_link = 0
        body = self._soup.find(id="bodyContent")

        for link in body.find_all('a', class_=""):
            if not link.get('href').startswith('https://'):

                # Check if element's title starts with the black listed terms
                if not str(link.get('title')).startswith(tuple(black_list)):
                    id_link += 1
                    link_title = unquote(str(link.get('title')))
                    link = str(f"https://fr.wikipedia.org{link.get('href')}")
                    links.append({
                        'id': id_link, 'title': link_title, 'url': link
                    })
        return links



