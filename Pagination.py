from math import ceil
import questionary
from questionary import Style

custom_style_fancy = Style([
    ('qmark', 'fg:#673ab7 bold'),
    ('question', 'fg:#D6F49D bold'),
    ('answer', 'fg:#f44336 bold'),
    ('pointer', 'fg:#373D20 bold'),
    ('highlighted', 'fg:#D4CDF4 bold'),
    ('selected', 'fg:#DAFFEF bold'),
    ('text', 'fg:#FAC9B8 bold'),
])


def pagination(list_link, page, max_displayed=10):

    items = list_link[slice(max_displayed * (page - 1), page * max_displayed)],
    page = page,
    total_page = ceil(len(list_link) / max_displayed)
    test = []

    questionary.print(' Page actuelle: {} | Dernière page: {}'.format(str(page[0]), str(total_page)), style="bold fg:#F3947C")
    questionary.print('______________________________________', style="bold fg:lightblue")

    for i in items:
        for j in i:
            test.append(j['title'])

    test.append('~ Page suivante')
    test.append('~ Page precedente')

    answer = questionary.select(
        "Selectionnez un element de la liste",
        qmark='',
        instruction='👇',
        pointer='👉',
        choices=test,
        style=custom_style_fancy
    ).ask()

    if answer == '~ Page suivante':
        if total_page == page[0]:
            return -3
        else:
            return -1

    elif answer == '~ Page precedente':
        return -2

    for i in list_link:
        if answer == i['title']:
            return i['id']

