from WebPage import WebPage
import Pagination as pagination
import questionary


def start_game():
    tries_nbr = 0
    page = 1
    current_page = starting_page
    list_link = request.get_all_links()

    questionary.print("~~~~~~~~~~~~~~~~~~ Wikilinks Game ~~~~~~~~~~~~~~~~~~", style="bold italic fg:lightblue")
    while current_page != ending_page:

        questionary.print("Vous êtes actuellement sur la page : ", style="bold fg:lightgrey")
        questionary.print(f"      {current_page['title']} 👈", style="bold fg:#85D6AB")
        questionary.print(f"Vous devez rejoindre la page : ", style="bold fg:lightgrey")
        questionary.print(f"      {ending_page['title']} 🏁", style="bold fg:#85D6AB")
        questionary.print('______________________________________', style="bold fg:lightblue")

        if current_page == ending_page:
            print(f"Wah, vous avez réussi en : {tries_nbr}, coups, bravo!")
            break

        user_choice = pagination.pagination(list_link, page)
        if user_choice == -1:
            page += 1

        elif user_choice == -2:
            if page > 1:
                page -= 1

        elif user_choice == -3:
            page = 1

        else:
            new_request = WebPage()
            current_page = new_request.get_page(list_link[int(user_choice) - 1]['url'])
            list_link = new_request.get_all_links()
            tries_nbr += 1


request = WebPage()
starting_page = request.get_page()
ending_page = request.get_page()
start_game()
